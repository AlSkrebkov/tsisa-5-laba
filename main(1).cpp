#include <iostream>
#include <cmath>
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <iomanip>
#include <numeric>

std::vector<double> FindMax(const std::vector<std::vector<double>>& vec) //поиск максимума
{
    std::vector<double> res = {0,0,0,0};
    for (const auto& item : vec)
    {
        for (int i = 0 ; i < vec.size(); i++)
        {
            if (item[i] > res[i])
            {
                res[i] = item[i];
            }
        }
    }
    return res;
}

int First(const std::vector<std::vector<double>>& vec, int index, const std::vector<double> part)//замена критериев ограничениями
{
    std::cout << "Glavniy criteriy " << index << std::endl;
    std::cout << "Ogranicheniya: ";
    for(const auto& i : part)
        std::cout << i << " ";
    std::cout << std::endl;
    std::vector<double> max_vals = FindMax(vec);
    std::vector<int> indexes;
    for (int i = 0; i < vec.size(); i++)
    {
        int k = 0;
        for (int j = 0; j < vec[i].size(); j++)
        {
            if (j != index) {
                if (vec[i][j] < max_vals[j] * part[k])//part-ограничения
                    break;
                if (j == vec[i].size() - 1)
                    indexes.emplace_back(i);
                k++;
            }
        }
    }
    if (indexes.size() == 1)
        return indexes[0];
    if (indexes.empty())
    {
        int max = 0;
        for (int i = 1; i < vec.size(); i++)
        {
            if (vec[i][index] > vec[max][index])
                max = i;
        }
        return max;
    }
    int max = indexes[0];
    for (const int& i : indexes)
    {
        if (vec[i][index] > vec[max][index])
            max = i;
    }
    return max;
}

double Chebishev(const double& x1, const double& y1, const double& x2, const double& y2)//метрика Чебышева
{
    double dis_1 = std::abs(x1 - x2);
    double dis_2 = std::abs(y1 - y2);
    if (dis_1 > dis_2)
        return dis_1;
    return dis_2;
}

int Second(const std::vector<std::vector<double>>& vec, const int& first, const int& second)//формирование и сужение множества Парето
{
    double _x = 10;
    double _y = 10;
    int res = 0;
    double res_dis = Chebishev(_x, _y, vec[0][first], vec[0][second]);
    for (size_t i = 1; i < vec.size(); i++)
    {
        std::cout << "(" << vec[i][first] << ";" << vec[i][second] << ")" << std::endl;
        double dis = Chebishev(_x, _y, vec[i][first], vec[i][second]);
        if (dis < res_dis)
        {
            res = i;
            res_dis = dis;
        }
    }
    return res;
}


void NormalizeMassive(std::vector<std::vector<double>>& vec)//нормализация массива , а конкретней для нормализации весов
{
    for (size_t i = 0; i < vec[0].size(); i++)
    {
        double sum = 0;
        for (const auto & j : vec)
            sum += j[i];
        for (auto & j : vec)
            j[i] /= sum;
    }
    for (const auto & i : vec)
    {
        for (int j = 0; j < i.size(); j++)
            std::cout << std::setprecision(1) << i[j] << " ";
        std::cout << std::endl;
    }
}

void NormalizeMas(std::vector<double>& vec)
{
    double sum = 0;
    for (const double i : vec)
        sum += i;
    for(double & i : vec)
        i /= sum;
}

int Third(std::vector<std::vector<double>> vec, std::vector<double>& weights) //взвешивание и объединение критериев
{
    NormalizeMas(weights);
    for (const double weight : weights)
        std::cout << weight << " ";
    std::cout << "\nMulttiply on \n";
    NormalizeMassive(vec);
    std::vector<double> choice(weights.size());
    int res = 0;
    double max_val = 0;
    std::cout << "Result: ";
    for (size_t i = 0; i < vec.size(); i++)
    {
        for (size_t j = 0; j < weights.size(); j++)
            choice[i] += weights[j] * vec[i][j];
        std::cout << choice[i] << " ";
        if (i == 0 || choice[i] > max_val)
        {
            res = i;
            max_val = choice[i];
        }
    }
    std::cout << "\n";
    return res;
}

std::vector<double> Compare(const std::vector<double>& vec)//сравнение ну или анализ иерархий
{
    std::vector<double> result(vec.size());
    std::cout << "|   |";
    for (size_t i = 1 ; i <= vec.size(); i++)
    {
        std::cout << "  " << i << "  |";
    }
    std::cout << " Sum |\n";
    for (size_t i = 0; i < vec.size(); i++)
    {
        std::cout << "| " << i + 1 << " | ";
        for (size_t j = 0; j < vec.size(); j++)
        {
            if (vec[i] == vec[j])
            {
                std::cout << " 1  | ";
                result[i] += 1;
            } else if (vec[i] > vec[j])
            {
                if (vec[i] - vec[j] == 1)
                {
                    result[i] += 3;
                    std::cout << " 3  | ";
                } else if (vec[i] - vec[j] == 2)
                {
                    result[i] += 5;
                    std::cout << " 5  | ";
                } else
                {
                    std::cout << " 7  | ";
                    result[i] += 7;
                }
            } else {
                if (vec[j] - vec[i] == 1)
                {
                    std::cout << "1/3 | ";
                    result[i] += 0.333;
                } else if (vec[j] - vec[i] == 2)
                {
                    std::cout << "1/5 | ";
                    result[i] += 0.2;
                } else
                {
                    std::cout << "1/7 | ";
                    result[i] += 0.142;
                }
            }
        }
        std::cout << " " << result[i] << "   |\n";
    }
    NormalizeMas(result);//а конкретней вектора
    return result;
}

int Compare(const std::vector<std::vector<double>>& vec, const std::vector<double>& weights)//всё тот же чёртов метод анализа иерархий , а конкретней умножение матрицы на вектор
{
    std::vector<std::vector<double>> Comparison(vec[0].size());
    for (size_t i = 0 ; i < vec[0].size(); i++)
    {
        std::cout << i << std::endl;
        std::vector<double> column(vec.size());
        for (int l = 0; l < vec.size(); l++)
            column[l] = vec[l][i];
        Comparison[i] = Compare(column);
    }
    std::cout << "\n";
    std::vector<double> weights_compare = Compare(weights);
    std::vector<double> result_mas(weights_compare.size());
    int result;
    double max_val = 0;
    std::cout << "Result: ";
    for (size_t i = 0; i < Comparison.size(); i++)
    {
        for (size_t j = 0; j < weights_compare.size(); j++)
            result_mas[i] += weights_compare[j] * Comparison[i][j];
        std::cout << result_mas[i] << " ";
        if (i == 0 || result_mas[i] > max_val)
        {
            result = i;
            max_val = result_mas[i];
        }
    }
    std::cout << std::endl;
    return result;
}

int main()
{
    std::vector<std::vector<double>> Derevo(4);//массивы критериев
   Derevo[0] = {1,2,3,4};
   Derevo[1] = {2,2,2,2};
   Derevo[2] = {3,4,7,1};
   Derevo[3] = {8,9,9,8};
    std::vector<double> vec = {6, 5, 2, 1}; // Веса критериев
    int index = 1;
    std::vector<double> minimum = {0.5, 0.3, 0.1};
    int first_method =  First(Derevo, index, minimum);
    std::cout << "First method: " << first_method + 1 << "\n\n\n";

    int first_index = 0;
    int second_index = 1;
    int second_method = Second(Derevo, first_index, second_index);
    std::cout << "Second method: " << second_method << "\n\n\n";

    int third_method = Third(Derevo, vec);
    std::cout << "Third method: " << third_method << "\n\n\n";

    int fourth_method = Compare(Derevo, vec);
    std::cout << "Fourth method the best is " << fourth_method << "\n\n\n";
	system("pause");
    return 0;
}